<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(5)
            ->has(Photo::factory()
                ->count(3))->create();
        Photo::factory()->count(3)
            ->has(Comment::factory()
                ->count(3))->create();

    }
}
